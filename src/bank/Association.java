/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bank;

/**
 *
 * @author mymac
 */
public class Association {
    
    public static void main(String [] args){
        
        Bank bank = new Bank("TD");
        Employee emp = new Employee("John");
        
        System.out.println(emp.getEmployeeName() +" is an employee of "+bank.getBankName());
    }
    
}
